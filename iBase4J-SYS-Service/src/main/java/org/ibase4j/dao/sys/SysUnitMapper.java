package org.ibase4j.dao.sys;

import org.ibase4j.core.base.BaseMapper;
import org.ibase4j.model.sys.SysUnit;

/**
 * @author ShenHuaJie
 *
 */
public interface SysUnitMapper extends BaseMapper<SysUnit> {

}
