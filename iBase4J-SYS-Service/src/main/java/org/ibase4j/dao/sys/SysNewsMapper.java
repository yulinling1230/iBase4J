package org.ibase4j.dao.sys;

import org.ibase4j.core.base.BaseMapper;
import org.ibase4j.model.sys.SysNews;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-01-29
 */
public interface SysNewsMapper extends BaseMapper<SysNews> {

}